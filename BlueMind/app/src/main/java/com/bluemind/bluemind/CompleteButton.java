package com.bluemind.bluemind;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class CompleteButton extends AppCompatActivity {

    private static final int READ_REQUEST_CODE = 42;
    private Button upload, exit;
    private EditText editText1, editText2;
    JSONParser parser = new JSONParser();
    private static String link = "http://ayboxoffice.com/test.php";
    private static final String KEY_STATUS = "status";
    String user_id, submit_your_activity, how_was_the_activity;
    private int fileValue;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.completechallenge);
        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        AWSMobileClient.getInstance().initialize(this).execute();

        editText1 = (EditText) findViewById(R.id.editText1);
        editText2 = (EditText) findViewById(R.id.editText2);

        submit_your_activity = editText1.getText().toString();
        how_was_the_activity = editText2.getText().toString();

        upload = (Button) findViewById(R.id.uploadButton);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.setType("image/*");
                startActivityForResult(intent, READ_REQUEST_CODE);
            }
        });

        exit = (Button) findViewById(R.id.exitButton);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
                InsertData(user_id, submit_your_activity, how_was_the_activity);
                Intent exitIntent = new Intent(getApplicationContext(), ReviewScreen.class);
                startActivity(exitIntent);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent resultData) {

        // The ACTION_OPEN_DOCUMENT intent was sent with the request code
        // READ_REQUEST_CODE. If the request code seen here doesn't match, it's the
        // response to some other intent, and the code below shouldn't run at all.

        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "Media File Uploaded", Toast.LENGTH_LONG).show();
                }
            }, 2000);
        }
    }

    public void getData(){
        user_id = String.valueOf(120);
        submit_your_activity = editText1.getText().toString();
        how_was_the_activity = editText2.getText().toString();
    }

    public void InsertData(final String user_id, final String submit_your_activity, final String how_was_the_activity){

        class writeData extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {
                String uID = user_id;
                String submitData = submit_your_activity;
                String HWTactivity = how_was_the_activity;

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id", uID));
                nameValuePairs.add(new BasicNameValuePair("name", submitData));
                nameValuePairs.add(new BasicNameValuePair("description", HWTactivity));
                nameValuePairs.add(new BasicNameValuePair("pdf", ""));

                try {
                    HttpClient httpClient = new DefaultHttpClient();

                    HttpPost httpPost = new HttpPost(link);

                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    HttpResponse httpResponse = httpClient.execute(httpPost);

                    HttpEntity httpEntity = httpResponse.getEntity();


                } catch (ClientProtocolException e) {

                } catch (IOException e) {

                }
                return "Data Inserted Successfully";
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if(s!=null){
                    Toast.makeText(getApplicationContext(),s,Toast.LENGTH_SHORT).show();
                }
            }
        }
        writeData writeData = new writeData();
        writeData.execute(user_id, submit_your_activity, how_was_the_activity);

    }
}
